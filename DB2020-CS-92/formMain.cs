﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DB2020_CS_92
{
    public partial class formMain : Form
    {
        //Exported Code from external source _Start
        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;

        [DllImportAttribute("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);
        [DllImportAttribute("user32.dll")]
        public static extern bool ReleaseCapture();
        // _END

        private static formMain _instance;
        public static formMain Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new formMain();
                }
                return _instance;
            }
        }
        private formMain()
        {
            InitializeComponent();
            subMenuInitialState();
            to_ucManage();
            this.ControlBox = false;
        }

        private void subMenuInitialState()
        {
            pnlSubMenuGroups.Visible = false;
            pnlSubMenuManage.Visible = false;
            pnlSubMenuEvaluation.Visible = false;
        }

        public void to_ucManage()
        {
            if (!pnlContant.Controls.Contains(ucManage.Instance))
            {
                pnlContant.Controls.Add(ucManage.Instance);
                ucManage.Instance.Dock = DockStyle.Fill;
                ucManage.Instance.BringToFront();
            }
            else
            {
                ucManage.Instance.BringToFront();
            }
        }

        public void to_ucGroups()
        {
            if (!pnlContant.Controls.Contains(ucGroups.Instance))
            {
                pnlContant.Controls.Add(ucGroups.Instance);
                ucGroups.Instance.Dock = DockStyle.Fill;
                ucGroups.Instance.BringToFront();
            }
            else
            {
                ucGroups.Instance.BringToFront();
                ucGroups.Instance.RefreshUC();
            }
        }

        public void to_ucAddGroups()
        {
            if (!pnlContant.Controls.Contains(ucAddGroup.Instance))
            {
                pnlContant.Controls.Add(ucAddGroup.Instance);
                ucAddGroup.Instance.Dock = DockStyle.Fill;
                ucAddGroup.Instance.BringToFront();
            }
            else
            {
                ucAddGroup.Instance.refreshUC();
                ucAddGroup.Instance.BringToFront();
            }
        }

        private void btnManageStudents_Click(object sender, EventArgs e)
        {
            to_ucManage();
            ucManage.Instance.viewStudentPanel();
        }

        private void btnManageAdvisors_Click(object sender, EventArgs e)
        {
            to_ucManage();
            ucManage.Instance.viewAdvisorPanel();
        }

        private void btnManageProjects_Click(object sender, EventArgs e)
        {
            to_ucManage();
            ucManage.Instance.viewProjectPanel();
            
        }

        private void btnCloseForm_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btnRestoreForm_Click(object sender, EventArgs e)
        {
            if (this.WindowState != FormWindowState.Maximized)
            {
                this.FormBorderStyle = FormBorderStyle.None;
                this.MaximizedBounds = Screen.FromHandle(this.Handle).WorkingArea;
                this.WindowState = FormWindowState.Maximized;
            }
            else
            {
                this.FormBorderStyle = FormBorderStyle.Sizable;
                this.WindowState = FormWindowState.Normal;
            }
        }

        private void btnMinimizeForm_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void panel1_MouseMove(object sender, MouseEventArgs e)
        {
            //Exported Code from external source _Start
            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
            }
            // _END
        }

        private void panel1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (this.WindowState != FormWindowState.Maximized)
            {
                this.WindowState = FormWindowState.Maximized;
            }
            else
            {
                this.WindowState = FormWindowState.Normal;
            }
        }

        private void hideSubMenu()
        {
            if (pnlSubMenuGroups.Visible == true)
            {
                pnlSubMenuGroups.Visible = false;
            }
            if (pnlSubMenuManage.Visible == true)
            {
                pnlSubMenuManage.Visible = false;
            }
            if (pnlSubMenuEvaluation.Visible == true)
            {
                pnlSubMenuEvaluation.Visible = false;
            }
        }

        private void showSubMenu(Panel subMenu)
        {
            if (subMenu.Visible == false)
            {
                hideSubMenu();
                subMenu.Visible = true;
            }
            else
            {
                subMenu.Visible = false;
            }
        }

        private void btnManage_Click(object sender, EventArgs e)
        {
            showSubMenu(pnlSubMenuManage);
            pnlShowCliked.Height = btnManage.Height;
            pnlShowCliked.Top = btnManage.Top;
        }

        private void btnGroups_Click(object sender, EventArgs e)
        {
            showSubMenu(pnlSubMenuGroups);
            pnlShowCliked.Height = btnGroups.Height;
            pnlShowCliked.Top = btnGroups.Top;
        }

        private void btnEvaluations_Click(object sender, EventArgs e)
        {
            showSubMenu(pnlSubMenuEvaluation);
            pnlShowCliked.Height = btnEvaluations.Height;
            pnlShowCliked.Top = btnEvaluations.Top;
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btnManageGroups_Click(object sender, EventArgs e)
        {
            to_ucGroups();
        }

        private void btnAssignAdvisor_Click(object sender, EventArgs e)
        {
            if (!pnlContant.Controls.Contains(ucAssignAdvisor.Instance))
            {
                pnlContant.Controls.Add(ucAssignAdvisor.Instance);
                ucAssignAdvisor.Instance.Dock = DockStyle.Fill;
                ucAssignAdvisor.Instance.BringToFront();
            }
            else
            {
                ucAssignAdvisor.Instance.RefreshUC();
                ucAssignAdvisor.Instance.BringToFront();
            }
        }

        private void btnAssignProjects_Click(object sender, EventArgs e)
        {
            if (!pnlContant.Controls.Contains(ucAssignProject.Instance))
            {
                pnlContant.Controls.Add(ucAssignProject.Instance);
                ucAssignProject.Instance.Dock = DockStyle.Fill;
                ucAssignProject.Instance.BringToFront();
            }
            else
            {
                ucAssignProject.Instance.RefreshUC();
                ucAssignProject.Instance.BringToFront();
            }
        }

        private void btnManageEvaluations_Click(object sender, EventArgs e)
        {
            if (!pnlContant.Controls.Contains(ucEvaluation.Instance))
            {
                pnlContant.Controls.Add(ucEvaluation.Instance);
                ucEvaluation.Instance.Dock = DockStyle.Fill;
                ucEvaluation.Instance.BringToFront();
            }
            else
            {
                ucEvaluation.Instance.RefreshUC();
                ucEvaluation.Instance.BringToFront();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (!pnlContant.Controls.Contains(ucGroupEvaluation.Instance))
            {
                pnlContant.Controls.Add(ucGroupEvaluation.Instance);
                ucGroupEvaluation.Instance.Dock = DockStyle.Fill;
                ucGroupEvaluation.Instance.BringToFront();
            }
            else
            {
                ucGroupEvaluation.Instance.BringToFront();
                ucGroupEvaluation.Instance.RefreshUC();
            }
        }

        private void btnLogout_Click(object sender, EventArgs e)
        {
            MessageBox.Show("You have successfully logged out.");
            Application.Exit();
        }
    }
}
