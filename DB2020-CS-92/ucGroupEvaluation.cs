﻿using Microsoft.VisualBasic;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DB2020_CS_92
{
    public partial class ucGroupEvaluation : UserControl
    {
        private static ucGroupEvaluation _instance;
        public static ucGroupEvaluation Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new ucGroupEvaluation();
                }
                return _instance;
            }
        }
        private ucGroupEvaluation()
        {
            InitializeComponent();
            loadDataInCB();
            if (comboBox1.Items.Count > 0)
            {
                comboBox1.SelectedIndex = 0;
            }
            loadDataInDTV();
        }

        private void loadDataInCB()
        {
            try
            {
                comboBox1.Items.Clear();
                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd2 = new SqlCommand("SELECT Name FROM Evaluation", con);
                using (var rdr = cmd2.ExecuteReader())
                {
                    while (rdr.Read())
                    {
                        comboBox1.Items.Add(rdr.GetString(0));
                    }
                }
            }
            catch (Exception)
            {

            }
        }

        private void loadDataInDTV()
        {
            try
            {
                if (comboBox1.Items.Count > 0 && comboBox1.SelectedItem != null)
                {
                    var con = Configuration.getInstance().getConnection();
                    SqlCommand cmd = new SqlCommand("SELECT G.Id AS GroupId, GE.ObtainedMarks, CASE WHEN GE.EvaluationDate IS NULL THEN CONVERT(DATE, GETDATE()) ELSE GE.EvaluationDate END AS EvaluationDate, E.TotalMarks, E.TotalWeightage FROM [Group] G LEFT OUTER JOIN Evaluation E ON E.Name = @name LEFT OUTER JOIN GroupEvaluation GE ON GE.GroupId = G.Id AND GE.EvaluationId = E.Id", con);
                    cmd.Parameters.AddWithValue("@name", comboBox1.SelectedItem.ToString());
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    DataTable dt = new DataTable();
                    da.Fill(dt);
                    dataGridView1.DataSource = dt;
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Sorry, There Was An Error Loading The Data.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dataGridView1_CellClick_1(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                int value;
                String input = Interaction.InputBox("Please enter the marks obtained by the respective student.", "Obtained Marks", "", (Screen.PrimaryScreen.WorkingArea.Width - 250) / 2, (Screen.PrimaryScreen.WorkingArea.Height - 150) / 2);
                if (input != "")
                {
                    try
                    {
                        value = Convert.ToInt32(input);
                        if (value <= Convert.ToInt32(dataGridView1.Rows[e.RowIndex].Cells[3].Value))
                        {
                            var con = Configuration.getInstance().getConnection();

                            SqlCommand check = new SqlCommand("SELECT COUNT(*) FROM GroupEvaluation WHERE GroupId = @GroupId AND EvaluationId = (SELECT E.Id FROM Evaluation E WHERE NAME = @EvaluationId)", con);
                            check.Parameters.AddWithValue("@GroupId", Convert.ToInt32(dataGridView1.Rows[e.RowIndex].Cells[0].Value));
                            check.Parameters.AddWithValue("@EvaluationId", comboBox1.SelectedItem.ToString());
                            int count = (Int32)check.ExecuteScalar();

                            if (count == 0)
                            {
                                SqlCommand cmd = new SqlCommand("INSERT INTO GroupEvaluation VALUES (@GroupId, (SELECT E.Id FROM Evaluation E WHERE NAME = @EvaluationId), @ObtainedMarks, @Date)", con);
                                cmd.Parameters.AddWithValue("@GroupId", Convert.ToInt32(dataGridView1.Rows[e.RowIndex].Cells[0].Value));
                                cmd.Parameters.AddWithValue("@EvaluationId", comboBox1.SelectedItem.ToString());
                                cmd.Parameters.AddWithValue("@ObtainedMarks", value);
                                cmd.Parameters.AddWithValue("@Date", DateTime.Now.Date);
                                cmd.ExecuteNonQuery();
                                MessageBox.Show("Marks Added.");
                            }
                            else
                            {
                                SqlCommand cmd = new SqlCommand("UPDATE GroupEvaluation SET ObtainedMarks = @ObtainedMarks WHERE GroupId = @GroupId AND EvaluationId = (SELECT E.Id FROM Evaluation E WHERE NAME = @EvaluationId)", con);
                                cmd.Parameters.AddWithValue("@GroupId", Convert.ToInt32(dataGridView1.Rows[e.RowIndex].Cells[0].Value));
                                cmd.Parameters.AddWithValue("@EvaluationId", comboBox1.SelectedItem.ToString());
                                cmd.Parameters.AddWithValue("@ObtainedMarks", value);
                                cmd.ExecuteNonQuery();
                                MessageBox.Show("Marks Updated.");
                            }

                            loadDataInDTV();
                        }
                        else
                        {
                            MessageBox.Show("Sorry, Obtained Marks Can Not Be Greater than Total Marks.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                    catch (Exception)
                    {
                        MessageBox.Show("Sorry, There Was An Error Affecting The Data.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else
                {
                    try
                    {
                        var con = Configuration.getInstance().getConnection();
                        SqlCommand cmd = new SqlCommand("DELETE FROM GroupEvaluation WHERE GroupId = @GroupId AND EvaluationId = (SELECT E.Id FROM Evaluation E WHERE NAME = @EvaluationId)", con);
                        cmd.Parameters.AddWithValue("@GroupId", Convert.ToInt32(dataGridView1.Rows[e.RowIndex].Cells[0].Value));
                        cmd.Parameters.AddWithValue("@EvaluationId", comboBox1.SelectedItem.ToString());
                        cmd.ExecuteNonQuery();
                        loadDataInDTV();
                        MessageBox.Show("Marks Cleared.");
                    }
                    catch (Exception)
                    {
                        MessageBox.Show("Sorry, There Was An Error Clearing The Data.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            loadDataInDTV();
        }

        private void tbSearch_TextChanged(object sender, EventArgs e)
        {
            try
            {
                string txt = tbSearch.Text;
                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("SELECT G.Id AS GroupId, GE.ObtainedMarks, CASE WHEN GE.EvaluationDate IS NULL THEN CONVERT(DATE, GETDATE()) ELSE GE.EvaluationDate END AS EvaluationDate, E.TotalMarks, E.TotalWeightage FROM [Group] G LEFT OUTER JOIN Evaluation E ON E.Name = @name LEFT OUTER JOIN GroupEvaluation GE ON GE.GroupId = G.Id AND GE.EvaluationId = E.Id WHERE G.Id LIKE '%" + txt + "%' OR ObtainedMarks LIKE '%" + txt + "%' OR EvaluationDate LIKE '%" + txt + "%' OR TotalMarks LIKE '%" + txt + "%' OR TotalWeightage LIKE '%" + txt + "%'", con);
                cmd.Parameters.AddWithValue("@name", comboBox1.SelectedItem.ToString());
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                dataGridView1.DataSource = dt;

            }
            catch (Exception)
            {
                MessageBox.Show("Sorry, There Was An Error Loading The Data.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);             
            }
        }

        public void RefreshUC (){

            tbSearch.Text = "";
            loadDataInCB();
            loadDataInDTV();
        }
    }
}
