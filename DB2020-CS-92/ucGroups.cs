﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DB2020_CS_92
{
    public partial class ucGroups : UserControl
    {
        private static ucGroups _instance;
        private bool ColomnAdded = false;

        public static ucGroups Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new ucGroups();
                }
                return _instance;
            }
        }
        private ucGroups()
        {
            InitializeComponent();
            loadDataInDtv();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            formMain.Instance.to_ucAddGroups();
        }

        private void loadDataInDtv()
        {
            try
            {

                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("SELECT GS.GroupId, GS.StudentId, S.RegistrationNo, L.[Value] AS [Status], CONVERT(DATE, GS.AssignmentDate) AS [Assignment date], G.Created_On AS [Group Created] FROM [GROUP] G JOIN GroupStudent GS ON G.Id = GS.GroupId JOIN Student S ON StudentId = S.Id JOIN Lookup L ON Status = L.Id", con);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                dataGridView1.DataSource = dt;
                if (ColomnAdded == false)
                {
                    var col = new DataGridViewButtonColumn();
                    col.HeaderText = "Change Status";
                    col.Name = "ChangeStatus";
                    col.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
                    col.Text = "Change Status";
                    col.UseColumnTextForButtonValue = true;
                    dataGridView1.Columns.AddRange(new DataGridViewColumn[] { col });
                    var col2 = new DataGridViewButtonColumn();
                    col2.HeaderText = "Dispand";
                    col2.Name = "Dispand";
                    col2.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
                    col2.Text = "Dispand";
                    col2.UseColumnTextForButtonValue = true;
                    dataGridView1.Columns.AddRange(new DataGridViewColumn[] { col2 });
                    ColomnAdded = true;
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Sorry, There Was An Error Loading The Data.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void AdjustColumnOrder()
        {
            dataGridView1.Columns["ChangeStatus"].DisplayIndex = 6;
            dataGridView1.Columns["Dispand"].DisplayIndex = 7;
        }

        private void loadSeachedDataInDtv(string txt)
        {
            try
            {
                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd;
                if (chbGroupId.Checked == true)
                {
                    cmd = new SqlCommand("SELECT GS.GroupId, GS.StudentId, S.RegistrationNo, L.[Value] AS [Status], CONVERT(DATE, GS.AssignmentDate) AS [Assignment date], G.Created_On AS [Group Created] FROM [GROUP] G JOIN GroupStudent GS ON G.Id = GS.GroupId JOIN Student S ON StudentId = S.Id JOIN Lookup L ON Status = L.Id AND (GroupId LIKE '%" + txt + "%')", con);
                }
                else
                {
                    cmd = new SqlCommand("SELECT GS.GroupId, GS.StudentId, S.RegistrationNo, L.[Value] AS [Status], CONVERT(DATE, GS.AssignmentDate) AS [Assignment date], G.Created_On AS [Group Created] FROM [GROUP] G JOIN GroupStudent GS ON G.Id = GS.GroupId JOIN Student S ON StudentId = S.Id JOIN Lookup L ON Status = L.Id AND (GS.StudentId LIKE '%" + txt + "%' OR RegistrationNo LIKE '%" + txt + "%' OR GroupId LIKE '%" + txt + "%' OR L.Value LIKE '%" + txt + "%')", con);
                }
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                dataGridView1.DataSource = dt;
                AdjustColumnOrder();

            }
            catch (Exception)
            {

            }

        }

        private void tbSearch_TextChanged(object sender, EventArgs e)
        {
            loadSeachedDataInDtv(tbSearch.Text);
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                if (e.ColumnIndex == 0)
                {
                    try
                    {
                        int stdId = Convert.ToInt32(dataGridView1.Rows[e.RowIndex].Cells[3].Value);

                        var con = Configuration.getInstance().getConnection();
                        SqlCommand check_status = new SqlCommand("SELECT [Status] FROM GroupStudent WHERE StudentId = @StudentId", con);
                        check_status.Parameters.AddWithValue("@StudentId", stdId);
                        int status = (int)check_status.ExecuteScalar();

                        if (status == 3)
                        {
                            SqlCommand cmd = new SqlCommand("UPDATE GroupStudent SET [Status] = 4 WHERE StudentId = @StudentId", con);
                            cmd.Parameters.AddWithValue("@StudentId", stdId);
                            cmd.ExecuteNonQuery();
                        }
                        else
                        {
                            SqlCommand cmd = new SqlCommand("UPDATE GroupStudent SET [Status] = 3 WHERE StudentId = @StudentId", con);
                            cmd.Parameters.AddWithValue("@StudentId", stdId);
                            cmd.ExecuteNonQuery();
                        }
                        MessageBox.Show("Status Changed Successfully.", "Success");
                        loadDataInDtv();
                    }
                    catch (Exception)
                    {
                        MessageBox.Show("Sorry There Was An Error Changing the Status.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }

                if (e.ColumnIndex == 1)
                {
                    if (DialogResult.Yes == MessageBox.Show("Are You Sure You Want To Dispand This Group ?", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Warning))
                    {
                        try
                        {
                            int id = Convert.ToInt32(dataGridView1.Rows[e.RowIndex].Cells[2].Value);

                            var con = Configuration.getInstance().getConnection();
                            SqlCommand cmd = new SqlCommand("DELETE FROM GroupEvaluation WHERE GroupId = @GroupId", con);
                            cmd.Parameters.AddWithValue("@GroupId", id);
                            cmd.ExecuteNonQuery();

                            SqlCommand cmd2 = new SqlCommand("DELETE FROM GroupProject WHERE GroupId = @GroupId", con);
                            cmd2.Parameters.AddWithValue("@GroupId", id);
                            cmd2.ExecuteNonQuery();

                            SqlCommand cmd3 = new SqlCommand("DELETE FROM GroupStudent WHERE GroupId = @GroupId", con);
                            cmd3.Parameters.AddWithValue("@GroupId", id);
                            cmd3.ExecuteNonQuery();

                            SqlCommand cmd4 = new SqlCommand("DELETE FROM [Group] WHERE Id = @Id", con);
                            cmd4.Parameters.AddWithValue("@Id", id);
                            cmd4.ExecuteNonQuery();
                            loadDataInDtv();
                        }
                        catch (Exception)
                        {
                            MessageBox.Show("Sorry There Was An Error Dispanding This Group.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                }


            }
        }

        public void RefreshUC()
        {
            tbSearch.Text = "";
            loadDataInDtv();
        }

    }
}
