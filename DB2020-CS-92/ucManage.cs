﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using iTextSharp.text;
using iTextSharp.text.pdf;

namespace DB2020_CS_92
{
    public partial class ucManage : UserControl
    {
        private static ucManage _instance;

        private bool isStudent = true;
        private bool isAdvisor = false;
        private bool isProject = false;

        private string contactNum;
        private string projectTitle;

        private bool isDeletableStudent;
        private bool isDeletableAdvisor;
        private bool isDeletableProject;

        private int idManage;

        private string checkFirstName;
        private string checkLastName;
        private string checkContact;
        private string checkRegNoAndSalart;
        private string checkEmail;
        private string checkDesignation;
        private bool checkGenderMale;
        private DateTime checkDateOfBirth;

        public static ucManage Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new ucManage();
                }
                return _instance;
            }
        }

        private ucManage()
        {
            InitializeComponent();
            viewDtvTable();
            lbDate.Text = "Date: " + DateTime.Now.Date.ToShortDateString();
            lbDesignation.Visible = false;
            cbDesignation.Visible = false;
            btnAdd.Enabled = true;
            btnEdit.Enabled = false;
            btnDelete.Enabled = false;
            cbDesignation.Text = "";
        }

        public void viewAdvisorPanel()
        {
            clearFields();
            lbCrudPnlTitle.Text = "Advisor";
            lbRegNoAndSalary.Text = "Salary";
            lbDesignation.Visible = true;
            cbDesignation.Visible = true;
            if (cbDesignation.Items.Count > 0)
            {
                cbDesignation.SelectedIndex = 0;
            }
            tbRegNoAndSalary.Mask = "";
            panel4.BringToFront();
            isStudent = false;
            isAdvisor = true;
            isProject = false;
            viewDtvTable();
        }

        public void viewProjectPanel()
        {
            if (!pnlManageCRUD.Controls.Contains(ucProject.Instance))
            {
                pnlManageCRUD.Controls.Add(ucProject.Instance);
                ucProject.Instance.Dock = DockStyle.Fill;
                ucProject.Instance.BringToFront();
            }
            else
            {
                ucProject.Instance.BringToFront();
            }
            lbCrudPnlTitle.Text = "Project";
            isStudent = false;
            isAdvisor = false;
            isProject = true;
            clearFields();
            viewDtvTable();
        }

        public void viewStudentPanel()
        {
            lbCrudPnlTitle.Text = "Student";
            lbRegNoAndSalary.Text = "Registration Number";
            lbDesignation.Visible = false;
            cbDesignation.Visible = false;
            panel4.BringToFront();
            isStudent = true;
            isAdvisor = false;
            isProject = false;
            clearFields();
            viewDtvTable();
        }

        private void viewDtvTable()
        {
            if (isStudent == true && isAdvisor == false && isProject == false)
            {
                try
                {

                    var con = Configuration.getInstance().getConnection();
                    SqlCommand cmd = new SqlCommand("SELECT (FirstName+' '+LastName) AS Name, RegistrationNo, Contact, Email, CONVERT(date, DateOfBirth) AS[Date Of Birth], CASE WHEN Gender <> -1 THEN(SELECT Value FROM Lookup L WHERE L.Id = P.Gender) END AS Gender From Person P Join Student S On P.id = S.Id;", con);
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    DataTable dt = new DataTable();
                    da.Fill(dt);
                    dtvManage.DataSource = dt;
                }
                catch (Exception)
                {
                    MessageBox.Show("Sorry, There Was An Error Loading The Data.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

            }
            else if (isProject == true && isAdvisor == false && isStudent == false)
            {
                try
                {
                    var con = Configuration.getInstance().getConnection();
                    SqlCommand cmd = new SqlCommand("Select Title, Description from Project", con);
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    DataTable dt = new DataTable();
                    da.Fill(dt);
                    dtvManage.DataSource = dt;
                }
                catch (Exception)
                {
                    MessageBox.Show("Sorry, There Was An Error Loading The Data.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else if (isAdvisor == true && isProject == false && isStudent == false)
            {
                try
                {
                    var con = Configuration.getInstance().getConnection();
                    SqlCommand cmd = new SqlCommand("SELECT(FirstName + ' ' + LastName) AS Name, Contact, Email, CONVERT(date, DateOfBirth) AS[Date Of Birth], CASE WHEN Gender <> -1 THEN(SELECT Value FROM Lookup L WHERE L.Id = P.Gender) END AS Gender, CASE WHEN Designation <> -1 THEN(SELECT Value FROM Lookup L WHERE L.Id = A.Designation) END AS Designation, Salary From Person P Join Advisor A On P.id = A.Id;", con);
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    DataTable dt = new DataTable();
                    da.Fill(dt);
                    dtvManage.DataSource = dt;
                }
                catch (Exception)
                {
                    MessageBox.Show("Sorry, There Was An Error Loading The Data.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void clearFields()
        {
            idManage = -1;
            projectTitle = "";
            contactNum = "";
            if ((isStudent == true || isAdvisor == true) && isProject == false)
            {
                tbFirstName.Text = "";
                tbLastName.Text = "";
                tbContact.Text = "";
                tbRegNoAndSalary.Text = "";
                tbEmail.Text = "";
                dtpDateOfBirth.Value = DateTime.Now;
                rbtnMale.Checked = false;
                rbtnFemale.Checked = false;
                cbDesignation.Text = "";
            }
            else if (isProject == true && isAdvisor == false && isStudent == false)
            {
                ucProject.Instance.clearFields();
            }

            btnAdd.Enabled = true;
            btnEdit.Enabled = false;
            btnDelete.Enabled = false;
        }

        int getGender()
        {
            if (rbtnMale.Checked == true)
            {
                return 1;
            }
            return 2;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            if ((isStudent == true || isAdvisor == true) && isProject == false)
            {
                Int32 result;

                SqlCommand check_Contact = new SqlCommand("SELECT COUNT(*) FROM Person WHERE (Contact = @Contact)", con);
                check_Contact.Parameters.AddWithValue("@Contact", tbContact.Text);
                int ContactExist = (int)check_Contact.ExecuteScalar();

                SqlCommand check_Email = new SqlCommand("SELECT COUNT(*) FROM Person WHERE (Email = @Email)", con);
                check_Email.Parameters.AddWithValue("@Email", tbEmail.Text);
                int EmailExist = (int)check_Email.ExecuteScalar();


                if (ContactExist == 0 && EmailExist == 0)
                {
                    SqlCommand cmd = new SqlCommand("Insert into Person values (@FirstName, @LastName, @Contact, @Email, @DateOfBirth, @Gender)", con);
                    cmd.Parameters.AddWithValue("@FirstName", tbFirstName.Text);
                    cmd.Parameters.AddWithValue("@LastName", tbLastName.Text);
                    cmd.Parameters.AddWithValue("@Contact", tbContact.Text);
                    cmd.Parameters.AddWithValue("@Email", tbEmail.Text);
                    cmd.Parameters.AddWithValue("@DateOfBirth", dtpDateOfBirth.Value.Date);
                    cmd.Parameters.AddWithValue("@Gender", getGender());

                    SqlCommand id = new SqlCommand("SELECT Id FROM Person WHERE (Contact = @Contact) AND (Email = @Email)", con);
                    id.Parameters.AddWithValue("@Contact", tbContact.Text);
                    id.Parameters.AddWithValue("@Email", tbEmail.Text);

                    if (isStudent == true && isAdvisor == false && isProject == false)
                    {
                        try
                        {
                            cmd.ExecuteNonQuery();
                            result = (Int32)id.ExecuteScalar();
                            SqlCommand cmd2 = new SqlCommand("Insert into Student values (@Id, @RegistrationNo)", con);
                            cmd2.Parameters.AddWithValue("@Id", result);
                            cmd2.Parameters.AddWithValue("@RegistrationNo", tbRegNoAndSalary.Text);
                            cmd2.ExecuteNonQuery();

                            MessageBox.Show("Student Record Saved Successfully.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

                        }
                        catch (Exception)
                        {
                            MessageBox.Show("Sorry, There Was An Error Creating The Student.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            return;
                        }
                    }
                    else if (isAdvisor == true && isProject == false && isStudent == false)
                    {
                        try
                        {
                            cmd.ExecuteNonQuery();
                            SqlCommand id2 = new SqlCommand("SELECT Id FROM Lookup WHERE (Value = @Value)", con);
                            id2.Parameters.AddWithValue("@Value", cbDesignation.Text);
                            int designation = (Int32)id2.ExecuteScalar();

                            result = (Int32)id.ExecuteScalar();
                            SqlCommand cmd2 = new SqlCommand("Insert into Advisor values (@Id, @Designation, @Salary)", con);
                            cmd2.Parameters.AddWithValue("@Id", result);
                            cmd2.Parameters.AddWithValue("@Designation", designation);
                            cmd2.Parameters.AddWithValue("@Salary", Convert.ToDecimal(tbRegNoAndSalary.Text));
                            cmd2.ExecuteNonQuery();

                            MessageBox.Show("Advisor Record Saved Successfully.", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);

                        }
                        catch (Exception)
                        {
                            MessageBox.Show("Sorry, There Was An Error Creating The Advisor.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            return;
                        }
                    }
                    clearFields();
                    viewDtvTable();
                }
                else
                {
                    MessageBox.Show("Record Already Exists.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

            }
            else if (isProject == true && isAdvisor == false && isStudent == false)
            {
                string title = ucProject.Instance.getTitle();
                string description = ucProject.Instance.getDescription();

                SqlCommand check_Title = new SqlCommand("SELECT COUNT(*) FROM Project WHERE (Title = @Title)", con);
                check_Title.Parameters.AddWithValue("@Title", title);
                int TitleExist = (int)check_Title.ExecuteScalar();
                if (TitleExist == 0)
                {
                    if (title != "" && description != "")
                    {
                        try
                        {
                            SqlCommand cmd = new SqlCommand("Insert into Project values (@Description, @Title)", con);
                            cmd.Parameters.AddWithValue("@Description", description);
                            cmd.Parameters.AddWithValue("@Title", title);
                            cmd.ExecuteNonQuery();
                            clearFields();
                            viewDtvTable();
                        }
                        catch
                        {
                            MessageBox.Show("Sorry, An Error Has Occurred While Creating The Project Please Try Again.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                    else
                    {
                        MessageBox.Show("The Input Is Insuffient To Create A New Project.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else
                {
                    MessageBox.Show("Project With This Title Already Exists.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            clearFields();
        }

        private void dtvManage_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                if ((isStudent == true || isAdvisor == true) && isProject == false)
                {
                    contactNum = dtvManage.Rows[e.RowIndex].Cells[1].Value.ToString();
                    if (isStudent == true)
                    {
                        var con = Configuration.getInstance().getConnection();
                        SqlCommand id2 = new SqlCommand("SELECT Person.Id, FirstName,LastName,Contact,Email,DateOfBirth,RegistrationNo,Value FROM Person JOIN Student ON (Contact = @Contact) AND (Person.id = Student.Id) JOIN (Select Id, Value From Lookup) L1 ON Person.Gender = L1.Id", con);
                        id2.Parameters.AddWithValue("@Contact", contactNum);
                        var dr = id2.ExecuteReader();
                        dr.Read();

                        idManage = Convert.ToInt32(dr["Id"]);

                        tbFirstName.Text = dr["FirstName"].ToString();
                        tbLastName.Text = dr["LastName"].ToString();
                        tbContact.Text = dr["Contact"].ToString();
                        tbEmail.Text = dr["Email"].ToString();
                        dtpDateOfBirth.Value = Convert.ToDateTime(dr["DateOfBirth"]).Date;
                        string gen = dr["Value"].ToString();
                        tbRegNoAndSalary.Text = dr["RegistrationNo"].ToString();
                        if (gen == "Male")
                        {
                            checkGenderMale = true;
                            rbtnFemale.Checked = false;
                            rbtnMale.Checked = true;
                        }
                        else
                        {
                            checkGenderMale = false;
                            rbtnMale.Checked = false;
                            rbtnFemale.Checked = true;
                        }
                        dr.Close();

                        checkFirstName = tbFirstName.Text;
                        checkLastName = tbLastName.Text;
                        checkContact = tbContact.Text;
                        checkRegNoAndSalart = tbRegNoAndSalary.Text;
                        checkEmail = tbEmail.Text;
                        checkDesignation = "";
                        checkDateOfBirth = dtpDateOfBirth.Value;
                        isDeletableStudent = true;
                    }
                    else if (isAdvisor == true)
                    {
                        var con = Configuration.getInstance().getConnection();
                        SqlCommand id2 = new SqlCommand("SELECT Person.Id, FirstName,LastName,Contact,Email,DateOfBirth,Salary,Value,Val FROM Person JOIN Advisor ON (Contact = @Contact) AND (Person.id = Advisor.Id) JOIN (Select Id, Value From Lookup) L1 ON Person.Gender = L1.Id JOIN (Select Id, Value As Val From Lookup) L2  On Advisor.Designation = L2.Id", con);
                        id2.Parameters.AddWithValue("@Contact", contactNum);
                        var dr = id2.ExecuteReader();
                        dr.Read();

                        idManage = Convert.ToInt32(dr["Id"]);

                        tbFirstName.Text = dr["FirstName"].ToString();
                        tbLastName.Text = dr["LastName"].ToString();
                        tbContact.Text = dr["Contact"].ToString();
                        tbEmail.Text = dr["Email"].ToString();
                        dtpDateOfBirth.Value = Convert.ToDateTime(dr["DateOfBirth"]).Date;
                        string gen = dr["Value"].ToString();
                        tbRegNoAndSalary.Text = dr["Salary"].ToString();
                        cbDesignation.Text = dr["Val"].ToString();
                        if (gen == "Male")
                        {
                            checkGenderMale = true;
                            rbtnFemale.Checked = false;
                            rbtnMale.Checked = true;
                        }
                        else
                        {
                            checkGenderMale = false;
                            rbtnMale.Checked = false;
                            rbtnFemale.Checked = true;
                        }
                        dr.Close();

                        checkFirstName = tbFirstName.Text;
                        checkLastName = tbLastName.Text;
                        checkContact = tbContact.Text;
                        checkRegNoAndSalart = tbRegNoAndSalary.Text;
                        checkEmail = tbEmail.Text;
                        checkDesignation = cbDesignation.Text;
                        checkDateOfBirth = dtpDateOfBirth.Value;
                        isDeletableAdvisor = true;
                    }
                }
                else if (isProject == true && isAdvisor == false && isStudent == false)
                {
                    projectTitle = dtvManage.Rows[e.RowIndex].Cells[0].Value.ToString();
                    idManage = ucProject.Instance.setFields(projectTitle);
                }
            }
            btnAdd.Enabled = false;
            btnEdit.Enabled = true;
            btnDelete.Enabled = true;
        }

        public void setIsDeletableProject(bool value)
        {
            isDeletableProject = value;
        }

        public bool getIsDeletableProject()
        {
            return isDeletableProject;
        }

        public void loadSeachedInDtv(string txt)
        {
            var con = Configuration.getInstance().getConnection();
            if (isStudent == true)
            {
                try
                {
                    SqlCommand cmd = new SqlCommand("SELECT (FirstName+' '+LastName) AS Name, Contact, Email, CONVERT(date, DateOfBirth) AS[Date Of Birth], CASE WHEN Gender <> -1 THEN(SELECT Value FROM Lookup L WHERE L.Id = P.Gender) END AS Gender, RegistrationNo FROM Person P Join Student S On P.id = S.Id AND (RegistrationNo LIKE '%" + txt + "%' OR Contact LIKE '%" + txt + "%' OR Email LIKE '%" + txt + "%')", con);
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    DataTable dt = new DataTable();
                    da.Fill(dt);
                    dtvManage.DataSource = dt;

                }
                catch (Exception)
                {
                    MessageBox.Show("Sorry, There Was An Error Loading The Data.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else if (isAdvisor == true)
            {
                try
                {
                    SqlCommand cmd = new SqlCommand("SELECT(FirstName + ' ' + LastName) AS Name, Contact, Email, CONVERT(date, DateOfBirth) AS[Date Of Birth], CASE WHEN Gender <> -1 THEN(SELECT Value FROM Lookup L WHERE L.Id = P.Gender) END AS Gender, CASE WHEN Designation <> -1 THEN(SELECT Value FROM Lookup L WHERE L.Id = A.Designation) END AS Designation, Salary From Person P Join Advisor A On P.id = A.Id and (Contact LIKE '%" + txt + "%' OR Email Like '%" + txt + "%')", con);
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    DataTable dt = new DataTable();
                    da.Fill(dt);
                    dtvManage.DataSource = dt;

                }
                catch (Exception)
                {
                    MessageBox.Show("Sorry, There Was An Error Loading The Data.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else if (isProject == true)
            {
                try
                {
                    SqlCommand cmd = new SqlCommand("SELECT Title, Description FROM Project WHERE Title LIKE '%" + txt + "%'", con);
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    DataTable dt = new DataTable();
                    da.Fill(dt);
                    dtvManage.DataSource = dt;

                }
                catch (Exception)
                {
                    MessageBox.Show("Sorry, There Was An Error Loading The Data.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void tbSearch_TextChanged(object sender, EventArgs e)
        {
            loadSeachedInDtv(tbSearch.Text);
        }

        private void tbFirstName_TextChanged(object sender, EventArgs e)
        {
            if (checkFirstName == tbFirstName.Text)
            {
                if (isStudent == true)
                {
                    isDeletableStudent = true;
                }
                else if (isAdvisor == true)
                {
                    isDeletableAdvisor = true;
                }
            }
            else
            {
                if (isStudent == true)
                {
                    isDeletableStudent = false;
                }
                else if (isAdvisor == true)
                {
                    isDeletableAdvisor = false;
                }
            }
        }

        private void tbLastName_TextChanged(object sender, EventArgs e)
        {
            if (checkLastName == tbLastName.Text)
            {
                if (isStudent == true)
                {
                    isDeletableStudent = true;
                }
                else if (isAdvisor == true)
                {
                    isDeletableAdvisor = true;
                }
            }
            else
            {
                if (isStudent == true)
                {
                    isDeletableStudent = false;
                }
                else if (isAdvisor == true)
                {
                    isDeletableAdvisor = false;
                }
            }
        }

        private void tbContact_TextChanged(object sender, EventArgs e)
        {
            if (checkContact == tbContact.Text)
            {
                if (isStudent == true)
                {
                    isDeletableStudent = true;
                }
                else if (isAdvisor == true)
                {
                    isDeletableAdvisor = true;
                }
            }
            else
            {
                if (isStudent == true)
                {
                    isDeletableStudent = false;
                }
                else if (isAdvisor == true)
                {
                    isDeletableAdvisor = false;
                }
            }
        }

        private void tbRegNoAndSalary_TextChanged(object sender, EventArgs e)
        {
            if (checkRegNoAndSalart == tbRegNoAndSalary.Text)
            {
                if (isStudent == true)
                {
                    isDeletableStudent = true;
                }
                else if (isAdvisor == true)
                {
                    isDeletableAdvisor = true;
                }
            }
            else
            {
                if (isStudent == true)
                {
                    isDeletableStudent = false;
                }
                else if (isAdvisor == true)
                {
                    isDeletableAdvisor = false;
                }
            }
        }

        private void tbEmail_TextChanged(object sender, EventArgs e)
        {
            if (checkEmail == tbEmail.Text)
            {
                if (isStudent == true)
                {
                    isDeletableStudent = true;
                }
                else if (isAdvisor == true)
                {
                    isDeletableAdvisor = true;
                }
            }
            else
            {
                if (isStudent == true)
                {
                    isDeletableStudent = false;
                }
                else if (isAdvisor == true)
                {
                    isDeletableAdvisor = false;
                }
            }
        }

        private void dtpDateOfBirth_ValueChanged(object sender, EventArgs e)
        {
            if (checkDateOfBirth == dtpDateOfBirth.Value)
            {
                if (isStudent == true)
                {
                    isDeletableStudent = true;
                }
                else if (isAdvisor == true)
                {
                    isDeletableAdvisor = true;
                }
            }
            else
            {
                if (isStudent == true)
                {
                    isDeletableStudent = false;
                }
                else if (isAdvisor == true)
                {
                    isDeletableAdvisor = false;
                }
            }
        }

        private void cbDesignation_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (checkDesignation == cbDesignation.Text)
            {
                if (isStudent == true)
                {
                    isDeletableStudent = true;
                }
                else if (isAdvisor == true)
                {
                    isDeletableAdvisor = true;
                }
            }
            else
            {
                if (isStudent == true)
                {
                    isDeletableStudent = false;
                }
                else if (isAdvisor == true)
                {
                    isDeletableAdvisor = false;
                }
            }
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            if ((isStudent == true || isAdvisor == true) && isProject == false)
            {
                var con = Configuration.getInstance().getConnection();
                SqlCommand check_id = new SqlCommand("SELECT COUNT(*) FROM Person WHERE (Id = @Id)", con);
                check_id.Parameters.AddWithValue("@id", idManage);
                int idExist = (int)check_id.ExecuteScalar();

                if (idExist != 0)
                {
                    if (isStudent == true)
                    {
                        if (isDeletableStudent == false)
                        {

                            if (checkFirstName != tbFirstName.Text)
                            {
                                SqlCommand cmd = new SqlCommand("UPDATE Person SET FirstName = '" + tbFirstName.Text + "'  WHERE Id = " + idManage, con);
                                cmd.ExecuteNonQuery();
                            }
                            if (checkLastName != tbLastName.Text)
                            {
                                SqlCommand cmd = new SqlCommand("UPDATE Person SET LastName = '" + tbLastName.Text + "'  WHERE Id = " + idManage, con);
                                cmd.ExecuteNonQuery();
                            }
                            if (checkContact != tbContact.Text)
                            {
                                SqlCommand cmd = new SqlCommand("UPDATE Person SET Contact = '" + tbContact.Text + "'  WHERE Id = " + idManage, con);
                                cmd.ExecuteNonQuery();
                            }
                            if (checkRegNoAndSalart != tbRegNoAndSalary.Text)
                            {
                                SqlCommand cmd = new SqlCommand("UPDATE Student SET RegistrationNo = '" + tbRegNoAndSalary.Text + "'  WHERE Id = " + idManage, con);
                                cmd.ExecuteNonQuery();
                            }
                            if (checkEmail != tbEmail.Text)
                            {
                                SqlCommand cmd = new SqlCommand("UPDATE Person SET Email = '" + tbEmail.Text + "'  WHERE Id = " + idManage, con);
                                cmd.ExecuteNonQuery();
                            }
                            if (checkDateOfBirth != dtpDateOfBirth.Value)
                            {
                                SqlCommand cmd = new SqlCommand("UPDATE Person SET DateOfBirth = @DateOfBirth  WHERE Id = " + idManage, con);
                                cmd.Parameters.AddWithValue("@DateOfBirth", dtpDateOfBirth.Value.Date);
                                cmd.ExecuteNonQuery();
                            }
                            if (checkGenderMale != rbtnMale.Checked)
                            {
                                SqlCommand cmd = new SqlCommand("UPDATE Person SET Gender = " + getGender() + "  WHERE Id = " + idManage, con);
                                cmd.ExecuteNonQuery();
                            }
                            MessageBox.Show("Record Updated Successfully.", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            btnAdd.Enabled = true;
                            btnEdit.Enabled = false;
                            btnDelete.Enabled = false;
                            viewDtvTable();
                        }
                        else
                        {
                            MessageBox.Show("No Changes Detected.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }

                    }
                    else if (isAdvisor == true)
                    {
                        if (isDeletableAdvisor == false)
                        {
                            if (checkFirstName != tbFirstName.Text)
                            {
                                SqlCommand cmd = new SqlCommand("UPDATE Person SET FirstName = '" + tbFirstName.Text + "'  WHERE Id = " + idManage, con);
                                cmd.ExecuteNonQuery();
                            }
                            if (checkLastName != tbLastName.Text)
                            {
                                SqlCommand cmd = new SqlCommand("UPDATE Person SET LastName = '" + tbLastName.Text + "'  WHERE Id = " + idManage, con);
                                cmd.ExecuteNonQuery();
                            }
                            if (checkContact != tbContact.Text)
                            {
                                SqlCommand cmd = new SqlCommand("UPDATE Person SET Contact = '" + tbContact.Text + "'  WHERE Id = " + idManage, con);
                                cmd.ExecuteNonQuery();
                            }
                            if (checkRegNoAndSalart != tbRegNoAndSalary.Text)
                            {
                                SqlCommand cmd = new SqlCommand("UPDATE Advisor SET Salary = '" + tbRegNoAndSalary.Text + "'  WHERE Id = " + idManage, con);
                                cmd.ExecuteNonQuery();
                            }
                            if (checkEmail != tbEmail.Text)
                            {
                                SqlCommand cmd = new SqlCommand("UPDATE Person SET Email = '" + tbEmail.Text + "'  WHERE Id = " + idManage, con);
                                cmd.ExecuteNonQuery();
                            }
                            if (checkDateOfBirth != dtpDateOfBirth.Value)
                            {
                                SqlCommand cmd = new SqlCommand("UPDATE Person SET DateOfBirth = @DateOfBirth  WHERE Id = " + idManage, con);
                                cmd.Parameters.AddWithValue("@DateOfBirth", dtpDateOfBirth.Value.Date);
                                cmd.ExecuteNonQuery();
                            }
                            if (checkGenderMale != rbtnMale.Checked)
                            {
                                SqlCommand cmd = new SqlCommand("UPDATE Person SET Gender = " + getGender() + "  WHERE Id = " + idManage, con);
                                cmd.ExecuteNonQuery();
                            }
                            if (checkDesignation != cbDesignation.Text)
                            {
                                SqlCommand cmd = new SqlCommand("UPDATE Advisor SET Designation = " + cbDesignation.Text + "  WHERE Id = " + idManage, con);
                                cmd.ExecuteNonQuery();
                            }
                            MessageBox.Show("Record Updated Successfully.", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            viewDtvTable();
                            btnAdd.Enabled = true;
                            btnEdit.Enabled = false;
                            btnDelete.Enabled = false;
                        }
                        else
                        {
                            MessageBox.Show("No changes detected.", "Tip", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                }
                else
                {
                    MessageBox.Show("Please, Select A Record To Update First.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else if (isProject == true && isAdvisor == false && isStudent == false)
            {
                var con = Configuration.getInstance().getConnection();
                SqlCommand check_id = new SqlCommand("SELECT COUNT(*) FROM Project WHERE (Id = @Id)", con);
                check_id.Parameters.AddWithValue("@id", idManage);
                int idExist = (int)check_id.ExecuteScalar();

                if (idExist != 0)
                {
                    if (isDeletableProject == false)
                    {
                        try
                        {
                            if (checkGenderMale != rbtnMale.Checked)
                            {
                                SqlCommand cmd = new SqlCommand("UPDATE Project SET Title = '" + ucProject.Instance.getTitle() + "'  WHERE Id = " + idManage, con);
                                cmd.ExecuteNonQuery();
                            }
                            if (checkDesignation != cbDesignation.Text)
                            {
                                SqlCommand cmd = new SqlCommand("UPDATE Project SET Description = '" + ucProject.Instance.getDescription() + "'  WHERE Id = " + idManage, con);
                                cmd.ExecuteNonQuery();
                            }
                            MessageBox.Show("Record updated successfully.", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            viewDtvTable();
                            btnAdd.Enabled = true;
                            btnEdit.Enabled = false;
                            btnDelete.Enabled = false;
                        }
                        catch (Exception)
                        {

                        }
                    }
                    else
                    {
                        MessageBox.Show("No changes detected.", "Tip", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
                else
                {
                    MessageBox.Show("Please, Select A Record To Update First.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if ((isStudent == true || isAdvisor == true) && isProject == false)
            {
                var con = Configuration.getInstance().getConnection();
                SqlCommand check_id = new SqlCommand("SELECT COUNT(*) FROM Person WHERE (Id = @Id)", con);
                check_id.Parameters.AddWithValue("@id", idManage);
                int idExist = (int)check_id.ExecuteScalar();

                if (idExist != 0)
                {
                    if (isStudent == true && isDeletableStudent)
                    {
                        try
                        {
                            if (DialogResult.Yes == MessageBox.Show("Are You Sure You Want To Delete This Student ?", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Warning))
                            {
                                SqlCommand cmd = new SqlCommand("DELETE FROM Student WHERE (Id = @Id)", con);
                                cmd.Parameters.AddWithValue("@id", idManage);
                                SqlCommand cmd2 = new SqlCommand("DELETE FROM Person WHERE (Id = @Id)", con);
                                cmd2.Parameters.AddWithValue("@id", idManage);
                                SqlCommand cmd3 = new SqlCommand("DELETE FROM GroupStudent WHERE (StudentId = @Id)", con);
                                cmd3.Parameters.AddWithValue("@id", idManage);
                                cmd3.ExecuteNonQuery();
                                cmd.ExecuteNonQuery();
                                cmd2.ExecuteNonQuery();
                                MessageBox.Show("Record Deleted Successfully.", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                clearFields();
                                btnAdd.Enabled = true;
                                btnEdit.Enabled = false;
                                btnDelete.Enabled = false;
                                viewDtvTable();
                            }
                        }
                        catch (Exception)
                        {
                            MessageBox.Show("Sorry, There Was An Error Deleting The Record.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                    else if (isAdvisor == true && isDeletableAdvisor)
                    {
                        try
                        {
                            if (DialogResult.Yes == MessageBox.Show("Are You Sure You Want To Delete This Advisor ?", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Warning))
                            {
                                SqlCommand cmd = new SqlCommand("DELETE FROM Advisor WHERE (Id = @Id)", con);
                                cmd.Parameters.AddWithValue("@id", idManage);
                                SqlCommand cmd2 = new SqlCommand("DELETE FROM Person WHERE (Id = @Id)", con);
                                cmd2.Parameters.AddWithValue("@id", idManage);
                                SqlCommand cmd3 = new SqlCommand("DELETE FROM ProjectAdvisor WHERE (AdvisorId = @Id)", con);
                                cmd3.Parameters.AddWithValue("@id", idManage);
                                cmd3.ExecuteNonQuery();
                                cmd.ExecuteNonQuery();
                                cmd2.ExecuteNonQuery();
                                MessageBox.Show("Record Deleted Successfully.", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                clearFields();
                                btnAdd.Enabled = true;
                                btnEdit.Enabled = false;
                                btnDelete.Enabled = false;
                                viewDtvTable();
                            }
                        }
                        catch (Exception)
                        {
                            MessageBox.Show("Sorry, There Was An Error Deleting The Record.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                    else
                    {
                        MessageBox.Show("Please, Select A Record To Delete First.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else
                {
                    MessageBox.Show("Sorry, Record Not Found.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else if (isProject == true && isAdvisor == false && isStudent == false)
            {
                var con = Configuration.getInstance().getConnection();
                SqlCommand check_id = new SqlCommand("SELECT COUNT(*) FROM Project WHERE (Id = @Id)", con);
                check_id.Parameters.AddWithValue("@id", idManage);
                int idExist = (int)check_id.ExecuteScalar();

                if (idExist != 0 && isDeletableProject)
                {
                    try
                    {
                        if (DialogResult.Yes == MessageBox.Show("Are You Sure You Want To Delete This Project ?", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Warning))
                        {
                            SqlCommand cmd = new SqlCommand("DELETE FROM Project WHERE (Id = @Id)", con);
                            cmd.Parameters.AddWithValue("@Id", idManage);
                            SqlCommand cmd2 = new SqlCommand("DELETE FROM GroupProject WHERE (ProjectId = @Id)", con);
                            cmd2.Parameters.AddWithValue("@Id", idManage);
                            SqlCommand cmd3 = new SqlCommand("DELETE FROM ProjectAdvisor WHERE (ProjectId = @Id)", con);
                            cmd3.Parameters.AddWithValue("@Id", idManage);
                            cmd3.ExecuteNonQuery();
                            cmd2.ExecuteNonQuery();
                            cmd.ExecuteNonQuery();
                            MessageBox.Show("Record Deleted Successfully.", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            clearFields();
                            btnAdd.Enabled = true;
                            btnEdit.Enabled = false;
                            btnDelete.Enabled = false;
                            viewDtvTable();
                        }
                    }
                    catch (Exception)
                    {
                        MessageBox.Show("Sorry, There Was An Error Deleting The Record.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else
                {
                    MessageBox.Show("Please Select A Record To Delete.", "TIP", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }

        private void rbtnMale_CheckedChanged(object sender, EventArgs e)
        {
            if (rbtnMale.Checked == checkGenderMale)
            {
                if (isStudent == true)
                {
                    isDeletableStudent = true;
                }
                else if (isAdvisor == true)
                {
                    isDeletableAdvisor = true;
                }
            }
            else
            {
                if (isStudent == true)
                {
                    isDeletableStudent = false;
                }
                else if (isAdvisor == true)
                {
                    isDeletableAdvisor = false;
                }
            }
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            if (isStudent == true && isAdvisor == false && isProject == false)
            {
                try
                {
                    string filename = "StudentReport.pdf";
                    iTextSharp.text.Font font5 = iTextSharp.text.FontFactory.GetFont(FontFactory.HELVETICA, 8);
                    iTextSharp.text.Font font6 = iTextSharp.text.FontFactory.GetFont(FontFactory.HELVETICA, 10);
                    Document doc = new Document(iTextSharp.text.PageSize.A4, 10, 10, 42, 35);
                    PdfWriter wr = PdfWriter.GetInstance(doc, new FileStream(filename, FileMode.Create));
                    doc.Open();
                    PdfPTable table = new PdfPTable(dtvManage.Columns.Count);
                    for (int j = 0; j < dtvManage.Columns.Count; j++)
                    {
                        table.AddCell(new Phrase(dtvManage.Columns[j].HeaderText, font6));
                    }
                    table.HeaderRows = 1;
                    for (int i = 0; i < dtvManage.Rows.Count; i++)
                    {
                        for (int k = 0; k < dtvManage.Columns.Count; k++)
                        {
                            if (dtvManage[k, i].Value != null)
                            {
                                table.AddCell(new Phrase(dtvManage[k, i].Value.ToString(), font5));
                            }
                        }
                    }
                    doc.Add(table);
                    doc.Close();
                    System.Diagnostics.Process.Start(filename);
                }
                catch (Exception)
                {
                    MessageBox.Show("Sorry, Data Is Required To Generate Pdf Reports");
                }
            }
            else if (isAdvisor == true && isProject == false && isStudent == false)
            {
                try
                {
                    string filename = "AdvisorReport.pdf";
                    Document doc = new Document(iTextSharp.text.PageSize.A4, 10, 10, 42, 35);
                    PdfWriter wr = PdfWriter.GetInstance(doc, new FileStream("AdvisorReport.pdf", FileMode.Create));
                    doc.Open();
                    PdfPTable table = new PdfPTable(dtvManage.Columns.Count);
                    for (int j = 0; j < dtvManage.Columns.Count; j++)
                    {
                        table.AddCell(new Phrase(dtvManage.Columns[j].HeaderText));
                    }
                    table.HeaderRows = 1;
                    for (int i = 0; i < dtvManage.Rows.Count; i++)
                    {
                        for (int k = 0; k < dtvManage.Columns.Count; k++)
                        {
                            if (dtvManage[k, i].Value != null)
                            {
                                table.AddCell(new Phrase(dtvManage[k, i].Value.ToString()));
                            }
                        }
                    }
                    doc.Add(table);
                    doc.Close();
                    System.Diagnostics.Process.Start(filename);
                }
                catch (Exception)
                {
                    MessageBox.Show("Sorry, Data Is Required To Generate Pdf Reports");
                }
            }
            else if (isProject == true && isAdvisor == false && isStudent == false)
            {
                try
                {
                    string filename = "ProjectReport.pdf";
                    Document doc = new Document(iTextSharp.text.PageSize.A4, 10, 10, 42, 35);
                    PdfWriter wr = PdfWriter.GetInstance(doc, new FileStream("ProjectReport.pdf", FileMode.Create));
                    doc.Open();
                    PdfPTable table = new PdfPTable(dtvManage.Columns.Count);
                    for (int j = 0; j < dtvManage.Columns.Count; j++)
                    {
                        table.AddCell(new Phrase(dtvManage.Columns[j].HeaderText));
                    }
                    table.HeaderRows = 1;
                    for (int i = 0; i < dtvManage.Rows.Count; i++)
                    {
                        for (int k = 0; k < dtvManage.Columns.Count; k++)
                        {
                            if (dtvManage[k, i].Value != null)
                            {
                                table.AddCell(new Phrase(dtvManage[k, i].Value.ToString()));
                            }
                        }
                    }
                    doc.Add(table);
                    doc.Close();
                    System.Diagnostics.Process.Start(filename);
                }
                catch (Exception)
                {
                    MessageBox.Show("Sorry, Data Is Required To Generate Pdf Reports");
                }
            }
        }
    }
}