﻿
namespace DB2020_CS_92
{
    partial class formMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(formMain));
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.btnLogout = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.btnMinimizeForm = new System.Windows.Forms.Button();
            this.btnRestoreForm = new System.Windows.Forms.Button();
            this.btnCloseForm = new System.Windows.Forms.Button();
            this.pnlMain = new System.Windows.Forms.Panel();
            this.pnlContant = new System.Windows.Forms.Panel();
            this.pnlSideMenu = new System.Windows.Forms.Panel();
            this.pnlMenu1 = new System.Windows.Forms.Panel();
            this.pnlSubMenuEvaluation = new System.Windows.Forms.Panel();
            this.button2 = new System.Windows.Forms.Button();
            this.btnManageEvaluations = new System.Windows.Forms.Button();
            this.pnlShowCliked = new System.Windows.Forms.Panel();
            this.btnEvaluations = new System.Windows.Forms.Button();
            this.pnlSubMenuGroups = new System.Windows.Forms.Panel();
            this.btnAssignAdvisor = new System.Windows.Forms.Button();
            this.btnAssignProjects = new System.Windows.Forms.Button();
            this.btnManageGroups = new System.Windows.Forms.Button();
            this.btnGroups = new System.Windows.Forms.Button();
            this.btnExit = new System.Windows.Forms.Button();
            this.pnlMenuBottomMargin = new System.Windows.Forms.Panel();
            this.pnlSubMenuManage = new System.Windows.Forms.Panel();
            this.btnManageProjects = new System.Windows.Forms.Button();
            this.btnManageAdvisors = new System.Windows.Forms.Button();
            this.btnManageStudents = new System.Windows.Forms.Button();
            this.btnManage = new System.Windows.Forms.Button();
            this.pnlMenuLogo = new System.Windows.Forms.Panel();
            this.pnlHeader = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel1.SuspendLayout();
            this.pnlMain.SuspendLayout();
            this.pnlSideMenu.SuspendLayout();
            this.pnlMenu1.SuspendLayout();
            this.pnlSubMenuEvaluation.SuspendLayout();
            this.pnlSubMenuGroups.SuspendLayout();
            this.pnlSubMenuManage.SuspendLayout();
            this.pnlHeader.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.White;
            this.panel1.Controls.Add(this.panel3);
            this.panel1.Controls.Add(this.btnLogout);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.btnMinimizeForm);
            this.panel1.Controls.Add(this.btnRestoreForm);
            this.panel1.Controls.Add(this.btnCloseForm);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1000, 31);
            this.panel1.TabIndex = 15;
            this.panel1.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.panel1_MouseDoubleClick);
            this.panel1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.panel1_MouseMove);
            // 
            // panel3
            // 
            this.panel3.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panel3.BackgroundImage")));
            this.panel3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.panel3.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(31, 31);
            this.panel3.TabIndex = 20;
            // 
            // btnLogout
            // 
            this.btnLogout.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnLogout.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(244)))), ((int)(((byte)(244)))));
            this.btnLogout.FlatAppearance.BorderSize = 0;
            this.btnLogout.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.Control;
            this.btnLogout.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.ControlLight;
            this.btnLogout.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLogout.ForeColor = System.Drawing.Color.Black;
            this.btnLogout.Location = new System.Drawing.Point(797, -2);
            this.btnLogout.Name = "btnLogout";
            this.btnLogout.Size = new System.Drawing.Size(61, 29);
            this.btnLogout.TabIndex = 12;
            this.btnLogout.Text = "Logout";
            this.btnLogout.UseVisualStyleBackColor = false;
            this.btnLogout.Click += new System.EventHandler(this.btnLogout_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(30, 7);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(129, 13);
            this.label1.TabIndex = 13;
            this.label1.Text = "FYP Management System";
            // 
            // btnMinimizeForm
            // 
            this.btnMinimizeForm.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnMinimizeForm.BackColor = System.Drawing.Color.Transparent;
            this.btnMinimizeForm.FlatAppearance.BorderSize = 0;
            this.btnMinimizeForm.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.ControlLight;
            this.btnMinimizeForm.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.Control;
            this.btnMinimizeForm.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMinimizeForm.ForeColor = System.Drawing.Color.Black;
            this.btnMinimizeForm.Image = ((System.Drawing.Image)(resources.GetObject("btnMinimizeForm.Image")));
            this.btnMinimizeForm.Location = new System.Drawing.Point(864, -3);
            this.btnMinimizeForm.Name = "btnMinimizeForm";
            this.btnMinimizeForm.Size = new System.Drawing.Size(44, 29);
            this.btnMinimizeForm.TabIndex = 19;
            this.btnMinimizeForm.UseVisualStyleBackColor = false;
            this.btnMinimizeForm.Click += new System.EventHandler(this.btnMinimizeForm_Click);
            // 
            // btnRestoreForm
            // 
            this.btnRestoreForm.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnRestoreForm.BackColor = System.Drawing.Color.Transparent;
            this.btnRestoreForm.FlatAppearance.BorderSize = 0;
            this.btnRestoreForm.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.ControlLight;
            this.btnRestoreForm.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.Control;
            this.btnRestoreForm.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRestoreForm.ForeColor = System.Drawing.Color.Black;
            this.btnRestoreForm.Image = ((System.Drawing.Image)(resources.GetObject("btnRestoreForm.Image")));
            this.btnRestoreForm.Location = new System.Drawing.Point(910, -3);
            this.btnRestoreForm.Name = "btnRestoreForm";
            this.btnRestoreForm.Size = new System.Drawing.Size(44, 29);
            this.btnRestoreForm.TabIndex = 18;
            this.btnRestoreForm.UseVisualStyleBackColor = false;
            this.btnRestoreForm.Click += new System.EventHandler(this.btnRestoreForm_Click);
            // 
            // btnCloseForm
            // 
            this.btnCloseForm.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCloseForm.BackColor = System.Drawing.Color.Transparent;
            this.btnCloseForm.FlatAppearance.BorderSize = 0;
            this.btnCloseForm.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.ControlLight;
            this.btnCloseForm.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.Control;
            this.btnCloseForm.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCloseForm.ForeColor = System.Drawing.Color.Black;
            this.btnCloseForm.Image = ((System.Drawing.Image)(resources.GetObject("btnCloseForm.Image")));
            this.btnCloseForm.Location = new System.Drawing.Point(956, -2);
            this.btnCloseForm.Name = "btnCloseForm";
            this.btnCloseForm.Size = new System.Drawing.Size(44, 29);
            this.btnCloseForm.TabIndex = 15;
            this.btnCloseForm.UseVisualStyleBackColor = false;
            this.btnCloseForm.Click += new System.EventHandler(this.btnCloseForm_Click);
            // 
            // pnlMain
            // 
            this.pnlMain.Controls.Add(this.pnlContant);
            this.pnlMain.Controls.Add(this.pnlSideMenu);
            this.pnlMain.Controls.Add(this.pnlHeader);
            this.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlMain.Location = new System.Drawing.Point(0, 31);
            this.pnlMain.Name = "pnlMain";
            this.pnlMain.Size = new System.Drawing.Size(1000, 669);
            this.pnlMain.TabIndex = 16;
            // 
            // pnlContant
            // 
            this.pnlContant.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.pnlContant.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlContant.Location = new System.Drawing.Point(234, 10);
            this.pnlContant.Name = "pnlContant";
            this.pnlContant.Size = new System.Drawing.Size(766, 659);
            this.pnlContant.TabIndex = 2;
            // 
            // pnlSideMenu
            // 
            this.pnlSideMenu.BackColor = System.Drawing.Color.Transparent;
            this.pnlSideMenu.Controls.Add(this.pnlMenu1);
            this.pnlSideMenu.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnlSideMenu.Location = new System.Drawing.Point(0, 10);
            this.pnlSideMenu.Name = "pnlSideMenu";
            this.pnlSideMenu.Size = new System.Drawing.Size(234, 659);
            this.pnlSideMenu.TabIndex = 1;
            // 
            // pnlMenu1
            // 
            this.pnlMenu1.AutoScroll = true;
            this.pnlMenu1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(93)))), ((int)(((byte)(104)))), ((int)(((byte)(121)))));
            this.pnlMenu1.Controls.Add(this.pnlSubMenuEvaluation);
            this.pnlMenu1.Controls.Add(this.pnlShowCliked);
            this.pnlMenu1.Controls.Add(this.btnEvaluations);
            this.pnlMenu1.Controls.Add(this.pnlSubMenuGroups);
            this.pnlMenu1.Controls.Add(this.btnGroups);
            this.pnlMenu1.Controls.Add(this.btnExit);
            this.pnlMenu1.Controls.Add(this.pnlMenuBottomMargin);
            this.pnlMenu1.Controls.Add(this.pnlSubMenuManage);
            this.pnlMenu1.Controls.Add(this.btnManage);
            this.pnlMenu1.Controls.Add(this.pnlMenuLogo);
            this.pnlMenu1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlMenu1.Location = new System.Drawing.Point(0, 0);
            this.pnlMenu1.Name = "pnlMenu1";
            this.pnlMenu1.Size = new System.Drawing.Size(234, 659);
            this.pnlMenu1.TabIndex = 0;
            // 
            // pnlSubMenuEvaluation
            // 
            this.pnlSubMenuEvaluation.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(90)))), ((int)(((byte)(100)))));
            this.pnlSubMenuEvaluation.Controls.Add(this.button2);
            this.pnlSubMenuEvaluation.Controls.Add(this.btnManageEvaluations);
            this.pnlSubMenuEvaluation.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlSubMenuEvaluation.Location = new System.Drawing.Point(0, 500);
            this.pnlSubMenuEvaluation.Name = "pnlSubMenuEvaluation";
            this.pnlSubMenuEvaluation.Size = new System.Drawing.Size(234, 84);
            this.pnlSubMenuEvaluation.TabIndex = 17;
            // 
            // button2
            // 
            this.button2.Dock = System.Windows.Forms.DockStyle.Top;
            this.button2.FlatAppearance.BorderSize = 0;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.ForeColor = System.Drawing.Color.White;
            this.button2.Location = new System.Drawing.Point(0, 42);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(234, 42);
            this.button2.TabIndex = 4;
            this.button2.Text = "Groups Evaluation";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // btnManageEvaluations
            // 
            this.btnManageEvaluations.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnManageEvaluations.FlatAppearance.BorderSize = 0;
            this.btnManageEvaluations.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnManageEvaluations.ForeColor = System.Drawing.Color.White;
            this.btnManageEvaluations.Location = new System.Drawing.Point(0, 0);
            this.btnManageEvaluations.Name = "btnManageEvaluations";
            this.btnManageEvaluations.Size = new System.Drawing.Size(234, 42);
            this.btnManageEvaluations.TabIndex = 3;
            this.btnManageEvaluations.Text = "Manage Evaluation";
            this.btnManageEvaluations.UseVisualStyleBackColor = true;
            this.btnManageEvaluations.Click += new System.EventHandler(this.btnManageEvaluations_Click);
            // 
            // pnlShowCliked
            // 
            this.pnlShowCliked.BackColor = System.Drawing.Color.White;
            this.pnlShowCliked.Location = new System.Drawing.Point(0, 129);
            this.pnlShowCliked.Name = "pnlShowCliked";
            this.pnlShowCliked.Size = new System.Drawing.Size(2, 42);
            this.pnlShowCliked.TabIndex = 16;
            // 
            // btnEvaluations
            // 
            this.btnEvaluations.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnEvaluations.FlatAppearance.BorderSize = 0;
            this.btnEvaluations.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEvaluations.ForeColor = System.Drawing.Color.White;
            this.btnEvaluations.Image = ((System.Drawing.Image)(resources.GetObject("btnEvaluations.Image")));
            this.btnEvaluations.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnEvaluations.Location = new System.Drawing.Point(0, 458);
            this.btnEvaluations.Name = "btnEvaluations";
            this.btnEvaluations.Size = new System.Drawing.Size(234, 42);
            this.btnEvaluations.TabIndex = 15;
            this.btnEvaluations.Text = "  Evaluations";
            this.btnEvaluations.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnEvaluations.UseVisualStyleBackColor = true;
            this.btnEvaluations.Click += new System.EventHandler(this.btnEvaluations_Click);
            // 
            // pnlSubMenuGroups
            // 
            this.pnlSubMenuGroups.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(90)))), ((int)(((byte)(100)))));
            this.pnlSubMenuGroups.Controls.Add(this.btnAssignAdvisor);
            this.pnlSubMenuGroups.Controls.Add(this.btnAssignProjects);
            this.pnlSubMenuGroups.Controls.Add(this.btnManageGroups);
            this.pnlSubMenuGroups.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlSubMenuGroups.Location = new System.Drawing.Point(0, 334);
            this.pnlSubMenuGroups.Name = "pnlSubMenuGroups";
            this.pnlSubMenuGroups.Size = new System.Drawing.Size(234, 124);
            this.pnlSubMenuGroups.TabIndex = 14;
            // 
            // btnAssignAdvisor
            // 
            this.btnAssignAdvisor.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnAssignAdvisor.FlatAppearance.BorderSize = 0;
            this.btnAssignAdvisor.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAssignAdvisor.ForeColor = System.Drawing.Color.White;
            this.btnAssignAdvisor.Location = new System.Drawing.Point(0, 84);
            this.btnAssignAdvisor.Name = "btnAssignAdvisor";
            this.btnAssignAdvisor.Size = new System.Drawing.Size(234, 42);
            this.btnAssignAdvisor.TabIndex = 5;
            this.btnAssignAdvisor.Text = "Assign Advisor";
            this.btnAssignAdvisor.UseVisualStyleBackColor = true;
            this.btnAssignAdvisor.Click += new System.EventHandler(this.btnAssignAdvisor_Click);
            // 
            // btnAssignProjects
            // 
            this.btnAssignProjects.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnAssignProjects.FlatAppearance.BorderSize = 0;
            this.btnAssignProjects.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAssignProjects.ForeColor = System.Drawing.Color.White;
            this.btnAssignProjects.Location = new System.Drawing.Point(0, 42);
            this.btnAssignProjects.Name = "btnAssignProjects";
            this.btnAssignProjects.Size = new System.Drawing.Size(234, 42);
            this.btnAssignProjects.TabIndex = 4;
            this.btnAssignProjects.Text = "Assign Project";
            this.btnAssignProjects.UseVisualStyleBackColor = true;
            this.btnAssignProjects.Click += new System.EventHandler(this.btnAssignProjects_Click);
            // 
            // btnManageGroups
            // 
            this.btnManageGroups.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnManageGroups.FlatAppearance.BorderSize = 0;
            this.btnManageGroups.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnManageGroups.ForeColor = System.Drawing.Color.White;
            this.btnManageGroups.Location = new System.Drawing.Point(0, 0);
            this.btnManageGroups.Name = "btnManageGroups";
            this.btnManageGroups.Size = new System.Drawing.Size(234, 42);
            this.btnManageGroups.TabIndex = 3;
            this.btnManageGroups.Text = "Manage Groups";
            this.btnManageGroups.UseVisualStyleBackColor = true;
            this.btnManageGroups.Click += new System.EventHandler(this.btnManageGroups_Click);
            // 
            // btnGroups
            // 
            this.btnGroups.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnGroups.FlatAppearance.BorderSize = 0;
            this.btnGroups.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnGroups.ForeColor = System.Drawing.Color.White;
            this.btnGroups.Image = ((System.Drawing.Image)(resources.GetObject("btnGroups.Image")));
            this.btnGroups.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnGroups.Location = new System.Drawing.Point(0, 292);
            this.btnGroups.Name = "btnGroups";
            this.btnGroups.Size = new System.Drawing.Size(234, 42);
            this.btnGroups.TabIndex = 13;
            this.btnGroups.Text = " Groups";
            this.btnGroups.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnGroups.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnGroups.UseVisualStyleBackColor = true;
            this.btnGroups.Click += new System.EventHandler(this.btnGroups_Click);
            // 
            // btnExit
            // 
            this.btnExit.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnExit.FlatAppearance.BorderSize = 0;
            this.btnExit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExit.ForeColor = System.Drawing.Color.White;
            this.btnExit.Image = ((System.Drawing.Image)(resources.GetObject("btnExit.Image")));
            this.btnExit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnExit.Location = new System.Drawing.Point(0, 607);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(234, 42);
            this.btnExit.TabIndex = 11;
            this.btnExit.Text = "  Exit";
            this.btnExit.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnExit.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // pnlMenuBottomMargin
            // 
            this.pnlMenuBottomMargin.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(93)))), ((int)(((byte)(104)))), ((int)(((byte)(121)))));
            this.pnlMenuBottomMargin.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlMenuBottomMargin.Location = new System.Drawing.Point(0, 649);
            this.pnlMenuBottomMargin.Name = "pnlMenuBottomMargin";
            this.pnlMenuBottomMargin.Size = new System.Drawing.Size(234, 10);
            this.pnlMenuBottomMargin.TabIndex = 10;
            // 
            // pnlSubMenuManage
            // 
            this.pnlSubMenuManage.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(90)))), ((int)(((byte)(100)))));
            this.pnlSubMenuManage.Controls.Add(this.btnManageProjects);
            this.pnlSubMenuManage.Controls.Add(this.btnManageAdvisors);
            this.pnlSubMenuManage.Controls.Add(this.btnManageStudents);
            this.pnlSubMenuManage.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlSubMenuManage.Location = new System.Drawing.Point(0, 171);
            this.pnlSubMenuManage.Name = "pnlSubMenuManage";
            this.pnlSubMenuManage.Size = new System.Drawing.Size(234, 121);
            this.pnlSubMenuManage.TabIndex = 1;
            // 
            // btnManageProjects
            // 
            this.btnManageProjects.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnManageProjects.FlatAppearance.BorderSize = 0;
            this.btnManageProjects.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnManageProjects.ForeColor = System.Drawing.Color.White;
            this.btnManageProjects.Location = new System.Drawing.Point(0, 84);
            this.btnManageProjects.Name = "btnManageProjects";
            this.btnManageProjects.Size = new System.Drawing.Size(234, 42);
            this.btnManageProjects.TabIndex = 3;
            this.btnManageProjects.Text = "Projects";
            this.btnManageProjects.UseVisualStyleBackColor = true;
            this.btnManageProjects.Click += new System.EventHandler(this.btnManageProjects_Click);
            // 
            // btnManageAdvisors
            // 
            this.btnManageAdvisors.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnManageAdvisors.FlatAppearance.BorderSize = 0;
            this.btnManageAdvisors.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnManageAdvisors.ForeColor = System.Drawing.Color.White;
            this.btnManageAdvisors.Location = new System.Drawing.Point(0, 42);
            this.btnManageAdvisors.Name = "btnManageAdvisors";
            this.btnManageAdvisors.Size = new System.Drawing.Size(234, 42);
            this.btnManageAdvisors.TabIndex = 2;
            this.btnManageAdvisors.Text = "Advisors";
            this.btnManageAdvisors.UseVisualStyleBackColor = true;
            this.btnManageAdvisors.Click += new System.EventHandler(this.btnManageAdvisors_Click);
            // 
            // btnManageStudents
            // 
            this.btnManageStudents.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnManageStudents.FlatAppearance.BorderSize = 0;
            this.btnManageStudents.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnManageStudents.ForeColor = System.Drawing.Color.White;
            this.btnManageStudents.Location = new System.Drawing.Point(0, 0);
            this.btnManageStudents.Name = "btnManageStudents";
            this.btnManageStudents.Size = new System.Drawing.Size(234, 42);
            this.btnManageStudents.TabIndex = 1;
            this.btnManageStudents.Text = "Students";
            this.btnManageStudents.UseVisualStyleBackColor = true;
            this.btnManageStudents.Click += new System.EventHandler(this.btnManageStudents_Click);
            // 
            // btnManage
            // 
            this.btnManage.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnManage.FlatAppearance.BorderSize = 0;
            this.btnManage.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnManage.ForeColor = System.Drawing.Color.White;
            this.btnManage.Image = ((System.Drawing.Image)(resources.GetObject("btnManage.Image")));
            this.btnManage.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnManage.Location = new System.Drawing.Point(0, 129);
            this.btnManage.Name = "btnManage";
            this.btnManage.Size = new System.Drawing.Size(234, 42);
            this.btnManage.TabIndex = 0;
            this.btnManage.Text = "  Manage Entities";
            this.btnManage.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnManage.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnManage.UseVisualStyleBackColor = true;
            this.btnManage.Click += new System.EventHandler(this.btnManage_Click);
            // 
            // pnlMenuLogo
            // 
            this.pnlMenuLogo.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pnlMenuLogo.BackgroundImage")));
            this.pnlMenuLogo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pnlMenuLogo.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlMenuLogo.Location = new System.Drawing.Point(0, 0);
            this.pnlMenuLogo.Name = "pnlMenuLogo";
            this.pnlMenuLogo.Size = new System.Drawing.Size(234, 129);
            this.pnlMenuLogo.TabIndex = 0;
            // 
            // pnlHeader
            // 
            this.pnlHeader.BackColor = System.Drawing.Color.Transparent;
            this.pnlHeader.Controls.Add(this.panel2);
            this.pnlHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlHeader.Location = new System.Drawing.Point(0, 0);
            this.pnlHeader.Name = "pnlHeader";
            this.pnlHeader.Size = new System.Drawing.Size(1000, 10);
            this.pnlHeader.TabIndex = 0;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(37)))), ((int)(((byte)(48)))));
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1000, 10);
            this.panel2.TabIndex = 0;
            // 
            // formMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1000, 700);
            this.Controls.Add(this.pnlMain);
            this.Controls.Add(this.panel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(100, 70);
            this.Name = "formMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.pnlMain.ResumeLayout(false);
            this.pnlSideMenu.ResumeLayout(false);
            this.pnlMenu1.ResumeLayout(false);
            this.pnlSubMenuEvaluation.ResumeLayout(false);
            this.pnlSubMenuGroups.ResumeLayout(false);
            this.pnlSubMenuManage.ResumeLayout(false);
            this.pnlHeader.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Button btnLogout;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnMinimizeForm;
        private System.Windows.Forms.Button btnRestoreForm;
        private System.Windows.Forms.Button btnCloseForm;
        private System.Windows.Forms.Panel pnlMain;
        private System.Windows.Forms.Panel pnlContant;
        private System.Windows.Forms.Panel pnlSideMenu;
        private System.Windows.Forms.Panel pnlMenu1;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Panel pnlMenuBottomMargin;
        private System.Windows.Forms.Panel pnlSubMenuManage;
        private System.Windows.Forms.Button btnManage;
        private System.Windows.Forms.Panel pnlMenuLogo;
        private System.Windows.Forms.Panel pnlHeader;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button btnManageProjects;
        private System.Windows.Forms.Button btnManageAdvisors;
        private System.Windows.Forms.Button btnManageStudents;
        private System.Windows.Forms.Button btnEvaluations;
        private System.Windows.Forms.Panel pnlSubMenuGroups;
        private System.Windows.Forms.Button btnAssignProjects;
        private System.Windows.Forms.Button btnManageGroups;
        private System.Windows.Forms.Button btnGroups;
        private System.Windows.Forms.Panel pnlShowCliked;
        private System.Windows.Forms.Button btnAssignAdvisor;
        private System.Windows.Forms.Panel pnlSubMenuEvaluation;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button btnManageEvaluations;
    }
}

