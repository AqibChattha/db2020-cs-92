﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.VisualBasic;

namespace DB2020_CS_92
{
    public partial class ucAssignProject : UserControl
    {
        private static ucAssignProject _instance;

        private bool tbSearchChange = false;

        public static ucAssignProject Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new ucAssignProject();
                }
                return _instance;
            }
        }
        private ucAssignProject()
        {
            InitializeComponent();
            loadDataInDtvGroups();
            loadDataInDtvProjects();
        }

        private void loadDataInDtvGroups()
        {
            try
            {

                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("DELETE FROM[GROUP] WHERE Id <> ALL (SELECT GroupId FROM GroupStudent) ", con);
                cmd.ExecuteNonQuery();
                SqlCommand cmd1 = new SqlCommand("SELECT * FROM [Group] WHERE Id <> ALL(SELECT GroupId From GroupProject)", con);
                SqlDataAdapter da = new SqlDataAdapter(cmd1);
                DataTable dt = new DataTable();
                da.Fill(dt);
                dtvGroups.DataSource = dt;
            }
            catch (Exception)
            {
                MessageBox.Show("Sorry, There Was An Error Loading The Data.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void loadDataInDtvProjects()
        {
            try
            {

                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("SELECT * FROM Project", con);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                dtvProjects.DataSource = dt;
            }
            catch (Exception)
            {
                MessageBox.Show("Sorry, there was an error loading the data.");
            }
        }

        private void dtvGroups_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                textBox1.Text = dtvGroups.Rows[e.RowIndex].Cells[0].Value.ToString();
            }
        }

        private void dtvProjects_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                textBox2.Text = dtvProjects.Rows[e.RowIndex].Cells[0].Value.ToString();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (textBox1.Text != "" && textBox2.Text != "")
            {
                try
                {
                    var con = Configuration.getInstance().getConnection();
                    SqlCommand cmd = new SqlCommand("INSERT INTO GroupProject VALUES (@ProjectId, @GroupId, @AssignmentDate)", con);
                    cmd.Parameters.AddWithValue("@ProjectId", Convert.ToInt32(textBox2.Text));
                    cmd.Parameters.AddWithValue("@GroupId", Convert.ToInt32(textBox1.Text));
                    cmd.Parameters.AddWithValue("@AssignmentDate", DateTime.Now.Date);
                    cmd.ExecuteNonQuery();
                    MessageBox.Show("Project Assigned.", "Success");
                    loadDataInDtvGroups();
                }
                catch (Exception)
                {
                    MessageBox.Show("Invalid Input.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                MessageBox.Show("Please Enter The Input First.", "Tip", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {
            if (tbSearchChange == false)
            {
                textBox4.Text = "";
                tbSearchChange = true;
            }
            var con = Configuration.getInstance().getConnection();
            try
            {
                SqlCommand cmd = new SqlCommand("SELECT * FROM [Group] G WHERE G.Id LIKE '%" + textBox3.Text + "%' AND Id <> ALL(SELECT GroupId From GroupStudent)", con);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                dtvGroups.DataSource = dt;

            }
            catch (Exception)
            {
                MessageBox.Show("Sorry, There Was An Error Loading The Data.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void textBox4_TextChanged(object sender, EventArgs e)
        {
            if (tbSearchChange == true)
            {
                textBox3.Text = "";
                tbSearchChange = false;
            }
            var con = Configuration.getInstance().getConnection();
            try
            {
                SqlCommand cmd = new SqlCommand("SELECT * FROM Project P WHERE P.Id LIKE '%" + textBox4.Text + "%'", con);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                dtvProjects.DataSource = dt;

            }
            catch (Exception)
            {
                MessageBox.Show("Sorry, There Was An Error Loading The Data.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void RefreshUC()
        {
            textBox1.Text = "";
            textBox2.Text = "";
            textBox3.Text = "";
            textBox4.Text = "";
            loadDataInDtvGroups();
            loadDataInDtvProjects();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            RefreshUC();
        }
    }
}
