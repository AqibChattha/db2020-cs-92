﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DB2020_CS_92
{
    public partial class ucProject : UserControl
    {
        private static ucProject _instance;

        private string checkTitle;
        private string checkDescription;

        public static ucProject Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new ucProject();
                }
                return _instance;
            }
        }
        public ucProject()
        {
            InitializeComponent();
        }

        public String getTitle()
        {
            return tbTitle.Text;
        }

        public String getDescription()
        {
            return rtbDescription.Text;
        }

        public void clearFields()
        {
            tbTitle.Text = "";
            rtbDescription.Text = "";
        }

        public int setFields(string title)
        {
            int id = -1;
            try
            {
                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("SELECT * FROM Project WHERE (Title = @Title)", con);
                cmd.Parameters.AddWithValue("@Title", title);
                var dr = cmd.ExecuteReader();
                dr.Read();
                tbTitle.Text = dr["Title"].ToString();
                rtbDescription.Text = dr["Description"].ToString();
                id = Convert.ToInt32(dr["Id"]);
                dr.Close();
                ucManage.Instance.setIsDeletableProject(true);
                checkDescription = rtbDescription.Text;
                checkTitle = tbTitle.Text;
            }
            catch (Exception)
            {

                throw;
            }
            return id;
        }

        private void tbTitle_TextChanged(object sender, EventArgs e)
        {
            if (checkTitle == tbTitle.Text)
            {
                ucManage.Instance.setIsDeletableProject(true);
            }
            else
            {
                ucManage.Instance.setIsDeletableProject(false);
            }
        }

        private void rtbDescription_TextChanged(object sender, EventArgs e)
        {
            if (checkDescription == rtbDescription.Text)
            {
                ucManage.Instance.setIsDeletableProject(true);
            }
            else
            {
                ucManage.Instance.setIsDeletableProject(false);
            }
        }
    }
}
