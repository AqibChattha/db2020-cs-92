﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DB2020_CS_92
{
    public partial class ucAddGroup : UserControl
    {
        private static ucAddGroup _instance;

        private List<string> regNos = new List<string>();

        public static ucAddGroup Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new ucAddGroup();
                }
                return _instance;
            }
        }

        private ucAddGroup()
        {
            InitializeComponent();
            loadRegNoInDtv1();
            loadSelectedMembersInDtv2();
        }

        private void loadRegNoInDtv1()
        {
            try
            {
                string regNumString = "";
                if (regNos.Count > 0)
                {
                    regNumString += regNos[0];
                    for (int i = 1; i < regNos.Count; i++)
                    {
                        regNumString += "' AND S.RegistrationNo <> '" + regNos[i];
                    }

                }
                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("SELECT S.RegistrationNo FROM Student S WHERE S.Id <> ALL (SELECT StudentId FROM GroupStudent) AND S.RegistrationNo <> '" + regNumString +"'", con);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                dataGridView1.DataSource = dt;
        }
            catch (Exception)
            {
                MessageBox.Show("Sorry, There Was An Error Loading The Data.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void loadSelectedMembersInDtv2()
        {
            try
            {
                string regNumString = "";
                if (regNos.Count > 0)
                {
                    regNumString += regNos[0];
                    for (int i = 1; i < regNos.Count; i++)
                    {
                        regNumString += "' OR RegistrationNo = '" + regNos[i];
                    }

                }
                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("SELECT FirstName+' '+LastName AS [Name], RegistrationNo, Contact, Email, DateOfBirth, L.[Value] FROM Student S JOIN Person P ON S.Id = P.Id JOIN Lookup L ON L.Id = P.Gender WHERE RegistrationNo = '" + regNumString + "'", con);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                dataGridView2.DataSource = dt;
            }
            catch (Exception)
            {
                MessageBox.Show("Sorry, There Was An Error Loading The Data.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            string regNo;
            try
            {
                regNo = dataGridView1.Rows[e.RowIndex].Cells[0].Value.ToString();
                if (!regNos.Contains(regNo))
                {
                    regNos.Add(regNo);
                    tbSearch.Text = "";
                }
                else
                {
                    MessageBox.Show("This Student Is Already Selected.", "Duplicate Entry");
                }
            }
            catch (Exception)
            {
                return;
            }

            loadRegNoInDtv1();
            loadSelectedMembersInDtv2();

        }

        private void btnCreate_Click(object sender, EventArgs e)
        {
            if (regNos.Count > 0)
            {
                int groupId;
                try
                {
                    var con = Configuration.getInstance().getConnection();
                    SqlCommand cmd2 = new SqlCommand("INSERT INTO [Group] VALUES (GETDATE());", con);
                    cmd2.ExecuteNonQuery();
                    SqlCommand id2 = new SqlCommand("SELECT G.Id FROM [Group] G WHERE G.Id <> ALL(SELECT GroupId FROM GroupStudent)", con);
                    groupId = (Int32)id2.ExecuteScalar();

                    foreach (var item in regNos)
                    {
                        SqlCommand cmd = new SqlCommand("INSERT INTO GroupStudent VALUES(@GroupId, (SELECT Id FROM Student WHERE RegistrationNo = @RegistrationNo), 3, GETDATE())", con);
                        cmd.Parameters.AddWithValue("@GroupId", groupId);
                        cmd.Parameters.AddWithValue("@RegistrationNo", item);
                        cmd.ExecuteNonQuery();                    
                    }
                    MessageBox.Show("Group Has Beem Created Succesfully.", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    formMain.Instance.to_ucGroups();
                }
                catch (Exception)
                {
                    MessageBox.Show("Sorry, There Was An Error Creating The Group.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                MessageBox.Show("Please Select A Student Before Creating A Group.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }



        }

        public void refreshUC()
        {
            regNos.Clear();
            tbSearch.Text = "";

            loadRegNoInDtv1();
            loadSelectedMembersInDtv2();
        }

        private void dataGridView2_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            string regNo = dataGridView2.Rows[e.RowIndex].Cells[1].Value.ToString();
            if (regNos.Contains(regNo))
            {
                regNos.Remove(regNo);
                tbSearch.Text = "";
            }

            loadRegNoInDtv1();
            loadSelectedMembersInDtv2();

        }

        private void tbSearch_TextChanged(object sender, EventArgs e)
        {
            try
            {
                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("SELECT S.RegistrationNo FROM Student S WHERE S.Id <> ALL (SELECT StudentId FROM GroupStudent) AND S.RegistrationNo LIKE '%" + tbSearch.Text + "%'", con);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                dataGridView1.DataSource = dt;
            }
            catch (Exception)
            {
                MessageBox.Show("Sorry, There Was An Error Loading The Data.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
