﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace DB2020_CS_92
{
    public partial class ucAssignAdvisor : UserControl
    {
        private static ucAssignAdvisor _instance;
        private bool tbSearchChange = false;

        public static ucAssignAdvisor Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new ucAssignAdvisor();
                }
                return _instance;
            }
        }
        public ucAssignAdvisor()
        {
            InitializeComponent();
            loadDataInDtvAdvisors();
            loadDataInDtvProjects();
        }

        private void loadDataInDtvAdvisors()
        {
            try
            {

                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("SELECT A.Id, P.Firstname+' '+P.Lastname AS Name, L.Value FROM Advisor A JOIN Lookup L ON A.Designation = L.Id JOIN Person P ON A.Id = P.Id", con);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                dtvAdvisors.DataSource = dt;
            }
            catch (Exception)
            {
                MessageBox.Show("Sorry, There Was An Error Loading The Data.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void loadDataInDtvProjects()
        {
            try
            {

                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("SELECT * FROM Project", con);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                dtvProjects.DataSource = dt;
            }
            catch (Exception)
            {
                MessageBox.Show("Sorry, There Was An Error Loading The Data.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dtvAdvisors_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                textBox1.Text = dtvAdvisors.Rows[e.RowIndex].Cells[0].Value.ToString();
            }
        }

        private void dtvProjects_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                textBox2.Text = dtvProjects.Rows[e.RowIndex].Cells[0].Value.ToString();
            }
        }

        private int getAdvisorRoleId()
        {
            if (comboBox1.Text == "Main Advisor")
            {
                return 11;
            }
            else if (comboBox1.Text == "Co-Advisor")
            {
                return 12;
            }
            else if (comboBox1.Text == "Industry Advisor")
            {
                return 14;
            }
            return 0;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (textBox1.Text != "" && textBox2.Text != "" && getAdvisorRoleId() != 0)
            {
                try
                {
                    var con = Configuration.getInstance().getConnection();
                    SqlCommand cmd = new SqlCommand("INSERT INTO ProjectAdvisor VALUES (@AdvisorId, @ProjectId, @AdvisorRole, @AssignmentDate)", con);
                    cmd.Parameters.AddWithValue("@AdvisorId", Convert.ToInt32(textBox1.Text));
                    cmd.Parameters.AddWithValue("@ProjectId", Convert.ToInt32(textBox2.Text));
                    cmd.Parameters.AddWithValue("@AdvisorRole", getAdvisorRoleId());
                    cmd.Parameters.AddWithValue("@AssignmentDate", DateTime.Now.Date);
                    cmd.ExecuteNonQuery();
                    MessageBox.Show("Advisor Assigned.", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                catch (Exception)
                {
                    MessageBox.Show("Invalid Input.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                MessageBox.Show("Please Enter The Input First.", "Tip", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {
            if (tbSearchChange == false)
            {
                textBox4.Text = "";
                tbSearchChange = true;
            }
            var con = Configuration.getInstance().getConnection();
            try
            {
                SqlCommand cmd = new SqlCommand("SELECT A.Id, P.Firstname+' '+P.Lastname AS Name, L.Value FROM Advisor A JOIN Lookup L ON A.Designation = L.Id JOIN Person P ON A.Id = P.Id WHERE A.Id LIKE '%" + textBox3.Text + "%'", con);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                dtvAdvisors.DataSource = dt;
            }
            catch (Exception)
            {
                MessageBox.Show("Sorry, There Was An Error Loading The Data.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void textBox4_TextChanged(object sender, EventArgs e)
        {
            if (tbSearchChange == true)
            {
                textBox3.Text = "";
                tbSearchChange = false;
            }
            var con = Configuration.getInstance().getConnection();
            try
            {
                SqlCommand cmd = new SqlCommand("SELECT * FROM Project P WHERE P.Id LIKE '%" + textBox4.Text + "%'", con);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                dtvProjects.DataSource = dt;

            }
            catch (Exception)
            {
                MessageBox.Show("Sorry, There Was An Error Loading The Data.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        public void RefreshUC()
        {
            textBox1.Text = "";
            textBox2.Text = "";
            textBox3.Text = "";
            textBox4.Text = "";
            if (comboBox1.Items.Count > 0)
            {
                comboBox1.SelectedIndex = 0;
            }
            loadDataInDtvAdvisors();
            loadDataInDtvProjects();
        }

    }
}
