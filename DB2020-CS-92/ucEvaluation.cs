﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DB2020_CS_92
{
    public partial class ucEvaluation : UserControl
    {
        private int eval_id = -1;
        private static ucEvaluation _instance;
        public static ucEvaluation Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new ucEvaluation();
                }
                return _instance;
            }
        }
        private ucEvaluation()
        {
            InitializeComponent();
            button2.Enabled = false;
            loadDataInDTV();
        }

        private void loadDataInDTV()
        {
            try
            {
                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("SELECT * FROM Evaluation", con);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                dataGridView1.DataSource = dt;
            }
            catch (Exception)
            {
                MessageBox.Show("Sorry, There Was An Error Loading The Data.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("INSERT INTO Evaluation VALUES (@Name, @TotalMarks, @TotalWeightage)", con);
                cmd.Parameters.AddWithValue("@Name", textBox1.Text);
                cmd.Parameters.AddWithValue("@TotalMarks", Convert.ToInt32(textBox2.Text));
                cmd.Parameters.AddWithValue("@TotalWeightage", Convert.ToInt32(textBox3.Text));
                cmd.ExecuteNonQuery();
                clearFields();
                loadDataInDTV();
            }
            catch (Exception)
            {
                MessageBox.Show("Sorry, Please Enter The Correct Input.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void clearFields()
        {
            textBox1.Text = "";
            textBox2.Text = "";
            textBox3.Text = "";
            tbSearch.Text = "";
            eval_id = -1;
            button1.Enabled = true;
            button2.Enabled = false;
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                var con = Configuration.getInstance().getConnection();
                SqlCommand id2 = new SqlCommand("SELECT * FROM Evaluation WHERE Id = @Id", con);
                id2.Parameters.AddWithValue("@Id", Convert.ToInt32(dataGridView1.Rows[e.RowIndex].Cells[0].Value));
                var dr = id2.ExecuteReader();
                dr.Read();
                eval_id = Convert.ToInt32(dr["Id"]);
                textBox1.Text = dr["Name"].ToString();
                textBox2.Text = dr["TotalMarks"].ToString();
                textBox3.Text = dr["TotalWeightage"].ToString();
                dr.Close();
                button1.Enabled = false;
                button2.Enabled = true;

            }
            catch (Exception)
            {
            }

        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (eval_id > 0)
            {
                try
                {
                    var con = Configuration.getInstance().getConnection();
                    SqlCommand cmd = new SqlCommand("UPDATE Evaluation SET Name = '" + textBox1.Text + "', TotalMarks = " + textBox2.Text + ", TotalWeightage = " + textBox3.Text + " WHERE Id = @Id", con);
                    cmd.Parameters.AddWithValue("@Id", eval_id);
                    cmd.ExecuteNonQuery();
                    clearFields();
                    loadDataInDTV();
                    button1.Enabled = true;
                    button2.Enabled = false;
                }
                catch (Exception)
                {
                    MessageBox.Show("Sorry, There Was An Error Updating The Evaluation.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                MessageBox.Show("Please Choose The Evaluation You Want To Edit.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {

            if (eval_id > 0)
            {
                if (DialogResult.Yes == MessageBox.Show("Are You Sure You Want To Delete This Evaluation ?", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Warning))
                {
                    try
                    {
                        var con = Configuration.getInstance().getConnection();

                        SqlCommand cmd = new SqlCommand("DELETE FROM GroupEvaluation WHERE EvaluationId = @Id", con);
                        cmd.Parameters.AddWithValue("@Id", eval_id);
                        cmd.ExecuteNonQuery();

                        SqlCommand cmd2 = new SqlCommand("DELETE FROM Evaluation WHERE Id = @Id", con);
                        cmd2.Parameters.AddWithValue("@Id", eval_id);
                        cmd2.ExecuteNonQuery();
                        clearFields();
                        loadDataInDTV();
                        button1.Enabled = true;
                        button2.Enabled = false;
                    }
                    catch (Exception)
                    {
                        MessageBox.Show("Sorry, There Was An Error Deleting The Evaluation.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
            else
            {
                MessageBox.Show("Please Choose The Evaluation You Want To Delete.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            clearFields();
        }

        private void tbSearch_TextChanged(object sender, EventArgs e)
        {
            try
            {
                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("SELECT * FROM Evaluation WHERE Name LIKE '%" + tbSearch.Text + "%' OR TotalMarks LIKE '%" + tbSearch.Text + "%' OR TotalWeightage LIKE '%" + tbSearch.Text + "%'", con);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                dataGridView1.DataSource = dt;
            }
            catch (Exception)
            {

            }
        }

        public void RefreshUC()
        {
            clearFields();
            loadDataInDTV();
        }

    }
}